struct Lights {
  u32 dirLightAmt;
  u32 pointLightAmt;
  DirLight *dirLights;
  PointLight *pointLights;
};
